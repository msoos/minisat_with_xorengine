#include "XorEngine.h"

#include "core/Solver.h"

#include "xorengine/Xor.ipp"
#include "xorengine/XorDetector.ipp"
#include "xorengine/XorPropagator.ipp"
#include "xorengine/private/Logging.hpp"

#include "prooflogging/pbp/pbp_xor_proof.ipp"

namespace Minisat {
    void XorEngine::trackForDel(CRef ref, size_t trailPos) {
        while (toDelete.size() < trailPos + 1) {
            toDelete.push_back(CRef_Undef);
        }
        toDelete[trailPos] = ref;
    }

    void XorEngine::notifyBacktrack() {
        if (propagator) propagator->notifyBacktrack();
        while ((int) toDelete.size() > solver.trail.size()) {
            CRef ref = toDelete.back();
            toDelete.pop_back();
            if (ref != CRef_Undef) {
                solver.removeClause(ref);
            }
        }
    }

    void XorEngine::reloc(ClauseAllocator& to) {
        for (CRef& ref: toDelete) {
            if (ref != Minisat::CRef_Undef) {
                auto& c = solver.ca[ref];
                assert(solver.locked(c));
                solver.ca.reloc(ref, to);
            }
        }
    }

    void XorEngine::init() {
        auto proof = solver.proof.get();
        assert(proof != nullptr);

        detector = std::unique_ptr<xorp::XorDetector<xorengine::UsedTypes>>(
            new xorp::XorDetector<xorengine::UsedTypes>(
                    solver.nVars(),
                    proof
                ));

        for (int i = 0; i < solver.clauses.size(); ++i) {
            CRef ref = solver.clauses[i];
            detector->addClause(solver.ca[ref]);
        }
        for (int i = 0; i < solver.learnts.size(); ++i) {
            CRef ref = solver.learnts[i];
            detector->addClause(solver.ca[ref]);
        }
        auto xors = detector->findXors();

        printf("c statistic: xors_found %lu \n", xors.size());

        propagator = std::unique_ptr<xorp::XorPropagator<xorengine::UsedTypes>>(
            new xorp::XorPropagator<xorengine::UsedTypes>(
                adapter,
                xors,
                proof
            ));
    }

    CRef XorEngine::propagate() {
        if (!propagator) init();
        if (conflict == CRef_Undef) {
            if (propagator) propagator->propagate();
            if (conflict != CRef_Undef) {
                solver.qhead = solver.trail.size();
            }
        }
        CRef result = conflict;
        conflict = CRef_Undef;
        return result;
    }
}

namespace xorp {
namespace literal {
template <typename Var, typename Lit>
inline Var toVar(const Lit& lit) {
  return Minisat::var(lit);
}

template <typename Lit>
inline bool isNegated(const Lit& lit) {
  return Minisat::sign(lit);
}

template <typename Lit>
inline Lit negated(const Lit& lit) {
  return ~lit;
}

template <typename Lit>
inline bool equals(const Lit& a, const Lit& b) {
  return a == b;
}

template <typename Lit>
inline bool lessThan(const Lit& a, const Lit& b) {
  return a < b;
}

template <typename Lit>
inline Lit undef() {
  return Minisat::lit_Undef;
}
}  // namespace literal

namespace variable {
template <typename Var>
inline size_t toIndex(const Var& var) {
  return var;
}

template <typename Var>
inline Var fromIndex(size_t index) {
  return index;
}

template <typename Lit, typename Var>
inline Lit toLit(const Var& var) {
    return Minisat::mkLit(var, false);
}

template <typename Lit, typename Var>
inline Lit toLit(const Var& var, bool isNegated) {
    return Minisat::mkLit(var, isNegated);
}

template <typename Var>
inline bool equals(const Var& a, const Var& b) {
  return a == b;
}

template <typename Var>
inline bool lessThan(const Var& a, const Var& b) {
  return a < b;
}

}  // namespace variable

namespace clause {
template <typename Clause>
inline auto begin(Clause& clause) {
  return static_cast<const Minisat::Lit*>(clause);
}

template <typename Clause>
inline auto end(Clause& clause) {
  return begin(clause) + clause.size();
}

template <typename Clause>
inline proof::ConstraintId getId(Clause& c) {
    // todo fix detection to work with zero constraint Ids.
    return c.id();
}
}  // namespace clause

template <typename Types>
Solver<Types>::Solver(typename Types::Solver& _solver) : solver(_solver) {}

template <typename Types>
bool Solver<Types>::isSet(typename Types::Var v) {
  return solver.solver.value(v) != l_Undef;
}

template <typename Types>
bool Solver<Types>::isTrue(typename Types::Var v) {
  return solver.solver.value(v) == l_True;
}

template <typename Types>
typename Types::Lit Solver<Types>::trail(size_t pos) {
  return solver.solver.trail[pos];
}

template <typename Types>
size_t Solver<Types>::trailSize() {
  return solver.solver.trail.size();
}

template <typename Types>
void Solver<Types>::conflict(const Reason<typename Types::Lit>& reason) {
    if (reason.clause.size() == 0) {
        assert(solver.solver.decisionLevel() == 0);
        solver.conflict = Minisat::CRef_EmptyClause;
    } else {
        assert(reason.clause.size() > 1);

        Minisat::CRef  cr = solver.solver.ca.alloc(reason.clause, reason.id, true);
        solver.conflict = cr;
        solver.trackForDel(cr, trailSize());
    }
}

template <typename Types>
void Solver<Types>::enqueue(typename Types::Lit l,
                            const std::function<Reason<typename Types::Lit>(typename Types::Lit l)> getReason) {
    assert(solver.conflict == Minisat::CRef_Undef);

    solver.solver.n_xor_props += 1;
    size_t trailPos = trailSize();
    Minisat::LazyRef lazyRef([this, trailPos, getReason, l](){
        solver.solver.n_xor_reasons += 1;
        auto reason = getReason(l);
        if (reason.clause.size() > 1) {
            Minisat::CRef cr = this->solver.solver.ca.alloc(reason.clause, reason.id, true);
            assert(reason.clause[0] == l);
            this->solver.trackForDel(cr, trailPos);
            assert(cr != Minisat::CRef_Undef);

            return cr;
        } else {
            assert(this->solver.solver.decisionLevel() == 0);
            assert(reason.clause.size() == 1);
            return Minisat::CRef_Undef;
        }
    });

    if (solver.solver.decisionLevel() == 0) {
        // we never do conflict analysis over decision level 0 so we
        // need to make sure that the proof logging is triggered by
        // computing the reason.
        solver.solver.uncheckedEnqueue(l, lazyRef.reasonGen());
    } else {
        solver.solver.uncheckedEnqueue(l, lazyRef);
    }
}

template <typename Types>
typename Types::Var Solver<Types>::getFreshVar() {
    bool polarity = false;
    // fresh variables are only introduced for the proof so we do not
    // need to decide on them
    bool decisionVar = false;
    return solver.solver.newVar(polarity, decisionVar);
}

template <typename Types>
LitName Solver<Types>::getLitPrintName(typename Types::Lit lit) {
    int val = Minisat::var(lit) + 1;
    if (Minisat::sign(lit)) {
        val = -val;
    }
    return LitName(val);
}
}  // namespace xorp

// todo: get rid of this definition
template xorengine::UsedTypes::Lit variable::toLit<xorengine::UsedTypes::Lit, xorengine::UsedTypes::Var>(const xorengine::UsedTypes::Var&);
template xorengine::UsedTypes::Var literal::toVar<xorengine::UsedTypes::Var, xorengine::UsedTypes::Lit>(const xorengine::UsedTypes::Lit&);
template bool literal::isNegated<xorengine::UsedTypes::Lit>(const xorengine::UsedTypes::Lit& lit);
template xorengine::UsedTypes::Lit literal::negated<xorengine::UsedTypes::Lit>(const xorengine::UsedTypes::Lit& lit);


template struct xorp::Xor<xorengine::UsedTypes>;
template class xorp::XorDetector<xorengine::UsedTypes>;
template class xorp::XorPropagator<xorengine::UsedTypes>;
template class xorp::Solver<xorengine::UsedTypes>;
