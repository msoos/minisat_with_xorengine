#ifndef XorEngine_Solver_h
#define XorEngine_Solver_h

#include <memory>

#include "core/SolverTypes.h"

#include "xorengine/XorDetector.hpp"
#include "xorengine/XorPropagator.hpp"

namespace Minisat {
    class XorEngine;
    class Solver;
}

namespace xorengine {
    struct UsedTypes {
      using Lit = Minisat::Lit;
      using Var = Minisat::Var;
      using Clause = Minisat::Clause;
      using Solver = Minisat::XorEngine;
    };

    using Detector = xorp::XorDetector<xorengine::UsedTypes>;
    using Propagator = xorp::XorPropagator<xorengine::UsedTypes>;
}

namespace Minisat{
    class XorEngine {
        public:
            Minisat::Solver& solver;
            CRef conflict = CRef_Undef;
            xorp::Solver<xorengine::UsedTypes> adapter;

        private:
            std::unique_ptr<xorengine::Propagator> propagator;
            std::unique_ptr<xorengine::Detector> detector;

            std::vector<CRef> toDelete;

        public:
        XorEngine(Minisat::Solver& _solver)
            : solver(_solver)
            , adapter(*this)
        {}

        void init();

        void trackForDel(CRef ref, size_t trailPos);

        CRef propagate();

        void notifyBacktrack();

        void reloc(ClauseAllocator& to);
    };

} // namespace Minisat
#endif