#!/bin/bash

set -e

rm -rf ./CMakeCache*
rm -rf ./CMake*
rm -rf ./Make*
rm -rf minisat
rm -rf include
rm -rf tests
rm -rf ./cmake*
rm -rf CMakeFiles
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON ..
make -j26
