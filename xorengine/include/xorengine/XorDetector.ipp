#pragma once

#include "xorengine/XorDetector.hpp"

#include "xorengine/Xor.hpp"
#include "xorengine/SolverAdapter.hpp"
#include "xorengine/private/myXor.hpp"
#include "xorengine/private/assert.hpp"


#include "prooflogging/ConstraintId.hpp"
#include "prooflogging/xor_proof.hpp"

#include <vector>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <iostream>

namespace xorp {
typedef uint32_t BDDNodeRef;

class BDDNode {
public:
    BDDNodeRef left = 0; // true
    BDDNodeRef right = 0; // false
    BDDNodeRef parent = 0; // false
    size_t var;
    proof::ConstraintId constraintID = {0};
};

inline std::ostream& operator<< (std::ostream &out, const BDDNode &node) {
    out << "( l:" << node.left << " r:" << node.right
        << " p:" << node.parent << " var:" << node.var << " cId:" << node.constraintID << ")";
    return out;
}

class BDD {
public:
    std::vector<BDDNode> data;

    struct {
        size_t nOpen = 0;
    } stats;

    size_t nVars;
    bool rhs;

    size_t constraintCounter = 0;

    BDD() {
        data.emplace_back();
    }

    bool isRoot(BDDNode& node) {
        return &node == data.data();
    }

    bool isLeaf(BDDNode& node) {
        return node.constraintID != proof::ConstraintId::undef();
    }

    bool isEmpty(BDDNode& node) {
        assert(node.left != 0 || node.right == 0);
        return node.left == 0 && node.constraintID == proof::ConstraintId::undef();
    }

    BDDNode& get(BDDNodeRef ref) {
        assert(0 <= ref && ref < data.size());
        return data[ref];
    }

    BDDNodeRef newNode(BDDNodeRef parent) {
        data.emplace_back();
        data.back().parent = parent;
        return data.size() - 1;
    }


    std::unordered_set<size_t> getQueried(BDDNode& node){
        std::unordered_set<size_t> result;

        if (isEmpty(node) && isRoot(node)) {
            return result;
        }

        if (!isEmpty(node)) {
            result.insert(node.var);
        }

        BDDNode* current = &node;
        while (!isRoot(*current)) {
            current = &get(current->parent);
            result.insert(current->var);
        }

        return result;
    }

    bool isOkToBeOpen(BDDNodeRef ref) {
        assert(isEmpty(get(ref)));

        size_t nVars = 0;
        bool rhs = false;
        while (!isRoot(get(ref))) {
            BDDNode& parent = get(get(ref).parent);
            // maybe the node is not open because we found a clause
            // that covers it after creation of the node
            if (isLeaf(parent)) {
                return true;
            }
            if (parent.left == ref) {
                rhs ^= true;
            }
            nVars += 1;
            ref = get(ref).parent;
        }

        stats.nOpen += 1;
        return (nVars == this->nVars) && (rhs == this->rhs);
    }

    bool isHappy() {
        stats.nOpen = 0;
        for (BDDNodeRef ref = 0; ref < data.size(); ref++) {
            if (isEmpty(get(ref))) {
                if (!isOkToBeOpen(ref)) {
                    return false;
                }
            }
        }

        return true;
    }

    template<typename Types>
    void addClause(const typename Types::Clause& clause) {
        constraintCounter += 1;
        std::map<size_t, bool> lits;
        for (auto it = clause::begin(clause); it != clause::end(clause); ++it) {
            lits.insert(std::make_pair(variable::toIndex(literal::toVar<typename Types::Var>(*it)), literal::isNegated(*it)));
        }
        if (lits.size() == 1) {
            return;
        }

        std::vector<BDDNodeRef> open;
        open.push_back(0);

        while (open.size() != 0) {
            BDDNodeRef current = open.back();
            open.pop_back();

            BDDNode& node = get(current);

            if (isEmpty(node)) {
                auto queried = getQueried(node);

                auto it = lits.begin();
                for (; it != lits.end(); ++it) {
                    size_t var = it->first;
                    if (queried.find(var) == queried.end()) {
                        break;
                    }
                }

                if (it == lits.end()) {
                    node.constraintID = clause::getId(clause);
                } else {
                    BDDNodeRef left = newNode(current);
                    BDDNodeRef right = newNode(current);
                    // get new reference as newNode might invalidate previous reference
                    BDDNode& node = get(current);
                    node.var = it->first;
                    node.left = left;
                    node.right = right;
                    bool isNegated = it->second;
                    if (isNegated) {
                        open.push_back(node.left);
                    } else {
                        open.push_back(node.right);
                    }
                }
            } else if (!isLeaf(node)) {
                size_t var = node.var;
                auto it = lits.find(var);
                if (it == lits.end()) {
                    auto queried = getQueried(node);
                    bool isClosingNode = true;
                    for (auto pair: lits) {
                        size_t var = pair.first;
                        auto it = queried.find(var);
                        if (it == queried.end()) {
                            isClosingNode = false;
                            break;
                        }
                    }
                    if (isClosingNode) {
                        node.constraintID = clause::getId(clause);
                    } else {
                        open.push_back(node.left);
                        open.push_back(node.right);
                    }
                } else {
                    bool isNegated = it->second;
                    if (isNegated) {
                        open.push_back(node.left);
                    } else {
                        open.push_back(node.right);
                    }
                }
            }
        }
    }


    void addDotNodeStyle(std::ostream &out, BDDNode& node, size_t idx) {
        out << "  " << idx << " [label=\"";
        if (isLeaf(node)) {
            out << "c" << node.constraintID;
        } else if (isEmpty(node)) {
            out << "empty";
        } else {
            out << node.var;
        }
        out << "\"";

        if (isEmpty(node)) {
            if (isOkToBeOpen(idx)) {
                out << ",color=green,style=filled";
            } else {
                out << ",color=red,style=filled";
            }
        }

        out << "];\n";
    }


    void toDOT(std::ostream &out){
        out << "digraph G { \n";
        for (size_t i = 0; i < data.size(); i++) {
            BDDNode& node = get(i);
            addDotNodeStyle(out, node, i);

            if (!isLeaf(node) && !isEmpty(node)) {
                out << "  " << i << " -> " << node.left << " [label=\"t\"];\n";
                out << "  " << i << " -> " << node.right << " [label=\"f\"];\n";
            }
        }
        out << "}";
    }
};

inline std::ostream& operator<< (std::ostream &out, const BDD &bdd) {
    for (size_t i = 0; i < bdd.data.size(); i++) {
        out << i << ": " << bdd.data[i] << "\n";
    }
    return out;
}

template<typename Types>
class TempXor {
private:
    using Lit = typename Types::Lit;
    using Var = typename Types::Var;
    using Clause = typename Types::Clause;

public:
    std::unordered_map<size_t, size_t> vars;
    bool rhs = true;
    BDD proofTree;

    template<typename Iter>
    TempXor(Iter begin, Iter end) {
        size_t idx = 0;

        for (Iter it = begin; it != end; ++it) {
            rhs ^= literal::isNegated(*it);
            vars.insert(std::make_pair(variable::toIndex(literal::toVar<typename Types::Var>(*it)), idx));
            idx += 1;
        }

        proofTree.rhs = rhs;
        proofTree.nVars = vars.size();
    }


    bool addPotentialClause(const Clause& clause){
        // todo: maybe check if rhs is OK for performance
        size_t nVars = 0;
        bool rhs = true;
        for (auto it = clause::begin(clause); it != clause::end(clause); ++it) {
            nVars += 1;
            size_t var = variable::toIndex(literal::toVar<Var>(*it));
            if (literal::isNegated(*it)) {
                rhs ^= true;
            }
            auto find = vars.find(var);
            if (find == vars.end()) {
                return false;
            }
        }

        if (nVars == vars.size() && rhs != this->rhs) {
            return false;
        } else {
            proofTree.addClause<Types>(clause);
            return nVars == vars.size();
        }
    }
};

template<typename Types>
inline size_t lit2varIdx(typename Types::Lit literal) {
    return variable::toIndex(literal::toVar<typename Types::Var>(literal));
}

class BloomFilter {
private:
    typedef uint32_t StoreType;
    // prime used for hash function
    const StoreType prime = 31;

    // map value to a bit in store type
    StoreType hash(size_t value) {
        StoreType result = 1;
        return result << (value % prime);
    }

    StoreType value = 0;

public:
    template<typename Types, typename Iter>
    void addAll(Iter begin, Iter end) {
        for (Iter it = begin; it != end; ++it) {
            size_t idx = lit2varIdx<Types>(*it);
            value |= hash(idx);
        }
    }

    /*
     * return true if this set is subset of other set
     */
    bool subset(BloomFilter& other) {
        return (value | other.value) == other.value;
    }
};

template<typename Clause>
struct VarOccurence {
    Clause& ref;
    BloomFilter variables;
    bool used = false;
    size_t size;

    VarOccurence(Clause& _ref)
        : ref(_ref)
    {}
};


template<typename Types>
xorp::XorDetector<Types>::XorDetector(size_t maxVar, proof::Proof<Types>* _proof)
    : occursPtr(std::make_unique<OccursVec>(maxVar + 1))
    , occurs(*occursPtr.get())
    , proof(_proof)
{}

template<typename Types>
xorp::XorDetector<Types>::~XorDetector(){}

template<typename Types>
void xorp::XorDetector<Types>::addClause(typename Types::Clause& cls) {
    auto begin = clause::begin(cls);
    auto end = clause::end(cls);

    if (begin != end) {
        size_t idx = lit2varIdx<Types>(*begin);
        Occurence occurence(cls);
        occurence.variables.template addAll<Types>(begin, end);
        // todo this will not work in general:
        occurence.size = end - begin;
        occurs[idx].push_back(occurence);
    }
}

template<typename Types>
std::vector<Xor<Types>> xorp::XorDetector<Types>::findXors(){
    std::vector<Xor<Types>> xors;
    for (auto& occList: occurs) {
        for (Occurence& occurence: occList) {
            if (!occurence.used && minSize <= occurence.size && occurence.size <= maxSize) {
                TempXor<Types> maybeXor(clause::begin(occurence.ref), clause::end(occurence.ref));
                for (auto it = maybeXor.vars.begin(); it != maybeXor.vars.end(); ++it) {
                    size_t var = it->first;
                    for (Occurence& occurence: occurs[var]) {
                        bool isUsed = maybeXor.addPotentialClause(occurence.ref);
                        if (isUsed) {
                            occurence.used = true;
                        }
                    }
                }
                if (maybeXor.proofTree.isHappy()) {

                    LOG(stat) << "nOpen: " << maybeXor.proofTree.stats.nOpen << ", size: " << maybeXor.vars.size() << EOM;

                    // std::ofstream stream("out.dot");
                    // maybeXor.proofTree.toDOT(stream);

                    std::vector<typename Types::Lit> lits;
                    for (auto it = maybeXor.vars.begin(); it != maybeXor.vars.end(); ++it) {
                        lits.push_back(variable::toLit<Lit>(variable::fromIndex<Var>(it->first)));
                    }

                    xors.emplace_back(maybeXor.rhs, std::move(lits));
                    Xor<Types>& x = xors.back();
                    if (proof != nullptr) {
                        x.id = std::move(proof::xr::newXorHandleFromProofTree<Types>(*proof, x, maybeXor.proofTree));
                    }
                }
            }
        }
    }
    return xors;
}

} // close namespace