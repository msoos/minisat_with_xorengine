#pragma once

#include "Xor.hpp"

template<typename Types>
xorp::Xor<Types>::~Xor() = default;