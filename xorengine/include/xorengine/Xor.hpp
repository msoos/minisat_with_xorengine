#pragma once

#include <vector>
#include "prooflogging/xor_proof.hpp"
#include "xorengine/private/assert.hpp"
#include "xorengine/SolverAdapter.hpp"

namespace xorp {

template<typename Types>
struct Xor {
private:
    using Lit = typename Types::Lit;

public:
    std::vector<Lit> lits;
    bool rhs;
    proof::xr::XorHandle<Types> id;

    Xor(bool _rhs, std::vector<Lit>&& _lits)
        :Xor(_rhs, std::move(_lits), proof::xr::XorHandle<Types>()){}

    Xor(bool _rhs, std::vector<Lit>&& _lits, proof::xr::XorHandle<Types>&& _id)
        : lits(std::move(_lits))
        , rhs(_rhs)
        , id(std::move(_id))
    {}

    ~Xor();

    bool operator==(const Xor<Types>& other) {
        if (other.rhs != this->rhs
                || other.lits.size() != this->lits.size()) {
            return false;
        }

        for (size_t i = 0; i < this->lits.size(); ++i) {
            assert(i == 0 || xorp::literal::lessThan(this->lits[i - 1], this->lits[i]));
            assert(i == 0 || xorp::literal::lessThan(other.lits[i - 1], other.lits[i]));
            if (!xorp::literal::equals(this->lits[i], other.lits[i])) {
                return false;
            }
        }
        return true;
    }
};

template<typename Lit>
inline std::ostream& operator<<(std::ostream& os, const Xor<Lit>& v) {
    for (auto lit: v.lits) {
        os << lit << " ";
    }
    os << "= " << v.rhs;
    return os;
}

} // end of namespace
