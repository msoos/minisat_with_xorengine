#pragma once
#include <functional>
#include <vector>
#include "prooflogging/ConstraintId.hpp"
#include "prooflogging/LitName.hpp"

namespace xorp {
    namespace literal {
        template<typename Var, typename Lit>
        Var toVar(const Lit& lit);

        template<typename Lit>
        bool isNegated(const Lit& lit);

        template<typename Lit>
        Lit negated(const Lit& lit);

        template<typename Lit>
        bool equals(const Lit& a, const Lit& b);

        template<typename Lit>
        bool lessThan(const Lit& a, const Lit& b);

        template<typename Lit>
        Lit undef();
    }

    namespace variable {
        template<typename Var>
        size_t toIndex(const Var& var);

        template<typename Var>
        Var fromIndex(size_t index);

        template<typename Lit, typename Var>
        Lit toLit(const Var& var);

        template<typename Lit, typename Var>
        Lit toLit(const Var& var, bool isNegated);

        template<typename Var>
        bool equals(const Var& a, const Var& b);

        template<typename Var>
        bool lessThan(const Var& a, const Var& b);
    }

    namespace clause {
        template<typename Clause>
        auto begin(Clause& clause);

        template<typename Clause>
        auto end(Clause& clause);

        template<typename Clause>
        proof::ConstraintId getId(Clause& clause);
    }

    template<typename Lit>
    struct Reason {
        std::vector<Lit> clause;
        proof::ConstraintId id;

        Reason() {}

        Reason(std::vector<Lit>&& _clause)
            : clause(_clause) {}
    };

    template<typename Lit>
    inline std::ostream& operator<<(std::ostream& os, const Reason<Lit>& v) {
        for (auto lit: v.clause) {
            os << lit << " ";
        }
        os << " id: " << v.id;
        return os;
    }


    template<typename Lit>
    using ReasonGenerator = std::function<Reason<Lit>(Lit l)>;

    template<typename Types>
    class Solver {
    private:
        typename Types::Solver& solver;

    public:
        explicit Solver(typename Types::Solver& _solver);

        bool isSet(typename Types::Var v);

        bool isTrue(typename Types::Var v);

        typename Types::Lit trail(size_t pos);

        size_t trailSize();

        void conflict(const Reason<typename Types::Lit>& reason);

        void enqueue(typename Types::Lit l, const ReasonGenerator<typename Types::Lit> getReason);

        LitName getLitPrintName(typename Types::Lit l);

        typename Types::Var getFreshVar();
    };
}