#include "xorengine/SolverAdapter.hpp"
#include "xorengine/private/myXor.hpp"
#include "xorengine/Xor.hpp"

#include "prooflogging/Proof.hpp"
#include "prooflogging/xor_proof.hpp"

using namespace xorp;


std::ostream& operator<<(std::ostream& os, const BitSet& v) {
    size_t counter = 0;

    BitSet::StoreType mask = BitSet::one << (BitSet::storeTypeSize - 1);
    for (auto x:v.data) {
        for (size_t i = 0; i < BitSet::storeTypeSize; i++) {
            if (x & mask) {
                os << "1";
            } else {
                os << "0";
            }
            x <<= 1;

            counter += 1;
            if (counter == 8) {
                os << " ";
                counter = 0;
            }
        }
        os << " ";
    }

    return os;
}

template<typename Types>
void XorMatrix<Types>::propagate(size_t col){
    LOG(trace) << "XorMatrix<Types>::propagate col: " << col << EOM;
    WatchList wl;

    // we do want to attach rows to the same watch list again
    // besides from keeping active watches this can happen
    // during a forced update
    wl.reserve(watches[col].size());
    std::swap(wl, watches[col]);

    assert(!unassigned[col]);
    for (WatchEntry<Types>& entry: wl) {
        entry.row->updateWatch(col);
    }
}

template<typename Types>
std::vector<size_t> XorMatrix<Types>::shrink() {
    std::vector<size_t> remainingCols;

    for (size_t col = 0; col < numCols; col++) {
        if (unassigned[col]) {
            remainingCols.push_back(col);
        }
    }

    for (XorRow<Types>& row: rows) {
        row.shrink(remainingCols);
    }

    numCols = remainingCols.size();

    return remainingCols;
}

template<typename Types>
void XorMatrix<Types>::initWatches() {
    LOG(debug) << "before initWatches" << EOM;
    for (XorRow<Types>& row: rows) {
        LOG(debug) << row.rhs << " = " << row.data << EOM;
    }

    for (XorRow<Types>& row: rows) {
        auto it = row.data.findAny();
        if (it != row.data.end()) {
            row.template updateBasicWatch<true>(it.col());
        } else {
            row.basicWatch = 0;
            row.nonBasicWatch = 1;
            if (row.rhs) {
                propagator.conflict(row);
            } else {
                row.del = true;
            }
        }

        LOG(debug) << "after new basic watch" << EOM;
        for (XorRow<Types>& row: rows) {
            LOG(debug) << row.rhs << " = " << row.data << EOM;
        }

    }

    LOG(debug) << "after initial diagonalization" << EOM;
    for (XorRow<Types>& row: rows) {
        LOG(debug) << row.rhs << " = " << row.data << EOM;
    }

    for (XorRow<Types>& row: rows) {
        if (row.del || row.isConflict()) {
            continue;
        }

        size_t maybeWatch = 0;
        if (row.basicWatch == maybeWatch) {
            maybeWatch = 1;
        }

        row.nonBasicWatch = maybeWatch;
        if (row[maybeWatch]) {
            this->watches[maybeWatch].push_back(WatchEntry<Types>(&row));
        } else {
            row.updateWatch(maybeWatch, true, row.basicWatch);

            if (row.basicWatch == row.nonBasicWatch) {
                row.del = true;
            }
        }
    }

    executeDeletion();
}

template<typename Types>
void XorMatrix<Types>::executeDeletion() {
    rows.erase(std::remove_if(rows.begin(), rows.end(),
    [](XorRow<Types>& row){
        if (row.del) {
            LOG(debug) << "deleting" << EOM;
        }
        return row.del;
    }),rows.end());

    // deletion invalidates pointers (rows might be removed or moved)
    // so we need to update the watchlist

    for (WatchList& wl:watches) {
        wl.clear();
    }

    for (XorRow<Types>& row:rows) {
        if (!row.isConflict()) {
            assert(watches.size() > row.basicWatch && watches.size() > row.nonBasicWatch);
            assert(row[row.basicWatch]);
            watches[row.basicWatch].emplace_back(&row);
            if (!row.isUnit()) {
                assert(row[row.nonBasicWatch]);
                watches[row.nonBasicWatch].emplace_back(&row);
            }
        }
    }
}

template<typename Types>
void XorPropagatorImpl<Types>::addVars(const Xor<Types>& c) {
    size_t mId = 0;

    for (typename Types::Lit lit: c.lits) {
        typename Types::Var var = literal::toVar<typename Types::Var>(lit);
        size_t index = xorp::variable::toIndex(var);
        if (index >= var2loc.size()) {
            var2loc.resize(index  + 1);
        }
        VarLocation& loc = var2loc[index];
        if (!loc.isContained) {
            loc.matrixId = mId;
            loc.col = col2var[mId].size();
            loc.isContained = true;
            col2var[mId].push_back(var);
        }
    }
}

template<typename Types>
void XorPropagatorImpl<Types>::addXor(const Xor<Types>& c) {
    size_t mId = 0;
    XorMatrix<Types>& matrix = matrices[mId];
    assert(mId == matrix.matrixId);

    matrix.rows.emplace_back(matrix);
    XorRow<Types>& row = matrix.rows.back();

    bool rhs = c.rhs;
    for (typename Types::Lit lit: c.lits) {
        typename Types::Var var = literal::toVar<typename Types::Var>(lit);
        // new variables should have been added via addVars
        size_t index = xorp::variable::toIndex(var);
        assert(index < var2loc.size());
        VarLocation& loc = var2loc[index];
        assert(loc.isContained);
        assert(loc.matrixId == matrix.matrixId);

        if (literal::isNegated(lit)) {
            rhs ^= true;
        }

        row.data.set(loc.col);
    }
    row.rhs = rhs;

    row.proof.set(matrix.col2xorHandle.size());
    matrix.col2xorHandle.push_back(c.id);
}

template<typename Types>
void XorPropagatorImpl<Types>::shrink() {
    for (XorMatrix<Types>& matrix: matrices) {
        updateAssignment(matrix);
        std::vector<size_t> remainingCols = matrix.shrink();
        size_t i = 0;
        for (size_t col = 0; col < matrix.numCols; col++) {
            typename Types::Var var = col2var[matrix.matrixId][col];
            if (col == remainingCols[i]) {
                col2var[matrix.matrixId][i] = var;
                var2loc[var].col = i;
                assert(var2loc[var].matrixId = matrix.matrixId);
                i += 1;
            } else {
                var2loc[var].isContained = false;
            }
        }
    }
}

template<typename Types>
XorPropagatorImpl<Types>::XorPropagatorImpl(
        XorPropagatorImpl<Types>::Solver& _solver,
        std::vector<Xor<Types>>& xors,
        proof::Proof<Types>* _proof
    )
    : solver(_solver)
    , proof(_proof)
{
    col2var.emplace_back();

    for (Xor<Types>& c: xors) {
        addVars(c);
    }

    matrices.emplace_back(*this, col2var.back().size(), xors.size(), 0);

    for (Xor<Types>& c: xors) {
        addXor(c);
    }
}

template<typename Types>
void XorPropagatorImpl<Types>::updateAssignment(XorMatrix<Types>& matrix) {
    matrix.unassigned.resetToTrue();
    matrix.value.reset();

    for (size_t i = 0; i < matrix.numCols; i++) {
        typename Types::Var var = col2var[matrix.matrixId][i];

        if (solver.isSet(var)) {
            matrix.unassigned.unset(i);
            if (solver.isTrue(var)) {
                matrix.value.set(i);
            }
        }
    }
}

template<typename Types>
std::vector<typename Types::Lit> XorPropagatorImpl<Types>::rowToClause(XorRow<Types>& row, bool isReason) {
    XorMatrix<Types>& matrix = *row.matrix;
    std::vector<typename Types::Var>& col2var = this->col2var[matrix.matrixId];

    // only variables set to true
    BitSet positive(row.data);
    positive &= matrix.unassigned;

    if (!isReason) {
        assert(positive.findAny() == positive.end());
    }

    positive ^= row.data;
    // only variables set to false
    BitSet negative(positive);
    positive &= matrix.value;
    negative ^= positive;


    std::vector<typename Types::Lit> clause;
    for (size_t col : positive) {
        typename Types::Lit l = xorp::variable::toLit<typename Types::Lit>(col2var[col]);
        l = xorp::literal::negated(l);
        clause.push_back(l);
    }

    for (size_t col : negative) {
        typename Types::Lit l = xorp::variable::toLit<typename Types::Lit>(col2var[col]);
        clause.push_back(l);
    }

    return clause;
}

template<typename Types>
proof::ConstraintId prooflogClauseFromRow(proof::Proof<Types>* proof, XorRow<Types>& row, const std::vector<typename Types::Lit>& clause) {
    if (proof != nullptr) {
        proof::xr::XorHandle<Types>* reasonXor = nullptr;
        if (row.proofHandle) {
            reasonXor = row.proofHandle.get();
        } else {
            XorMatrix<Types>& matrix = *row.matrix;
            std::vector<proof::xr::XorHandle<Types>> xors;
            for (size_t col: row.proof) {
                assert(matrix.col2xorHandle.size() > col);
                reasonXor = &matrix.col2xorHandle[col];
                xors.push_back(*reasonXor);
                // todo: this looks expensive, because we are copying
                // the xorhandle, which can be large for DRAT proofs.
            }
            assert(xors.size() > 0);

            if (xors.size() == 1) {
                reasonXor = reasonXor;
                // if the reason is based on one xor then suming does
                // not make sense, additionally, we do not want to set
                // row.proofHandle as this would result in deletion of
                // the xor when row.proofHandle is reset, note that
                // reasonXor already contains the right reference
            } else {
                row.proofHandle = std::make_unique<proof::xr::XorHandle<Types>>(proof::xr::xorSum(*proof, xors));
                reasonXor = row.proofHandle.get();
            }
        }

        assert(reasonXor != nullptr);
        return proof::xr::reasonGeneration(*proof, *reasonXor, clause);
    } else {
        return proof::ConstraintId::undef();
    }
}

template<typename Types>
Reason<typename Types::Lit> XorPropagatorImpl<Types>::computeReason(XorRow<Types>& row, typename Types::Lit propagatedLit) {
    XorMatrix<Types>& matrix = *row.matrix;
    XorPropagatorImpl<Types>& propagator = row.matrix->propagator;
    typename Types::Var var = literal::toVar<typename Types::Var>(propagatedLit);
    size_t index = xorp::variable::toIndex(var);
    auto loc = propagator.var2loc[index];
    assert(loc.matrixId == matrix.matrixId);
    size_t col = loc.col;

    assert(!matrix.unassigned[col]);
    matrix.unassigned.set(col);

    Reason<typename Types::Lit> reason;
    reason.clause = propagator.rowToClause(row, true);
    reason.clause.push_back(propagatedLit);
    std::swap(reason.clause.front(), reason.clause.back());

    matrix.unassigned.unset(col);

    reason.id = prooflogClauseFromRow(propagator.proof, row, reason.clause);
    return reason;
}

template<typename Types>
void XorPropagatorImpl<Types>::enqueue(size_t col, bool value, XorRow<Types>& row) {
    if (mute) return;
    XorMatrix<Types>& matrix = *row.matrix;
    std::vector<typename Types::Var>& col2var = this->col2var[matrix.matrixId];

    bool isNegated = !value;
    typename Types::Lit propagatedLit = xorp::variable::toLit<typename Types::Lit>(col2var[col]);
    if (isNegated) {
        propagatedLit = xorp::literal::negated(propagatedLit);
    }
    // LOG(trace) << "XorPropagatorImpl<Types>::enqueue "
    //     // todo
    //     // << logging::Wrapper([propagatedLit](std::ostream& log){
    //     //         xorp::literal::write(log, propagatedLit);})
    //     << solver.getLitPrintName(propagatedLit)
    //     << EOM;

    assert(matrix.unassigned[col]);

    matrix.value.setTo(col, value);
    matrix.unassigned.unset(col);

    solver.enqueue(propagatedLit, [&row](typename Types::Lit propagatedLit){
        XorPropagatorImpl<Types>& propagator = row.matrix->propagator;
        return propagator.computeReason(row, propagatedLit);
    });
}

template<typename Types>
void XorPropagatorImpl<Types>::notifyBacktrack() {
    mute = false;
    qhead = solver.trailSize();

    for (XorMatrix<Types>& matrix: matrices) {
        updateAssignment(matrix);
    }
}

template<typename Types>
void XorPropagatorImpl<Types>::conflict(XorRow<Types>& row) {
    if (mute) return;
    mute = true;
    Reason<typename Types::Lit> reason;
    reason.clause = rowToClause(row);
    reason.id = prooflogClauseFromRow(this->proof, row, reason.clause);
    solver.conflict(reason);

    LOG(trace) << "conflict" << EOM;
}

bool isVarInAnyMatrix(VarLocation& loc){
    return loc.isContained;
}

template<typename Types>
void XorPropagatorImpl<Types>::propagate() {
    if (!isInitialized) {
        isInitialized = true;
        for (XorMatrix<Types>& m: matrices) {
            m.initWatches();
        }
    }

    std::vector<std::pair<size_t, XorMatrix<Types>*>> toPropagate;

    while (!mute && (qhead < solver.trailSize() || !toPropagate.empty())) {
        // we first want to update our assignment state before doing
        // propagation, otherwise we could propagate something that is
        // already set by the solver
        while (qhead < solver.trailSize()) {
            typename Types::Lit lit = solver.trail(qhead);
            typename Types::Var var = literal::toVar<typename Types::Var>(lit);
            size_t index = xorp::variable::toIndex(var);
            if (index < var2loc.size()) {
                VarLocation loc = var2loc[index];
                if (isVarInAnyMatrix(loc)) {
                    XorMatrix<Types>& matrix = matrices[loc.matrixId];
                    matrix.unassigned.unset(loc.col);
                    assert(solver.isSet(var));
                    matrix.value.setTo(loc.col, solver.isTrue(var));

                    toPropagate.emplace_back(loc.col, &matrix);
                }
            }

            qhead += 1;
        }

        // now do one propagation step, as this might cause new
        // assignments we will need to update the state again
        if (!toPropagate.empty()) {
            auto& pair = toPropagate.back();
            pair.second->propagate(pair.first);
            toPropagate.pop_back();
        }
    }

    mute = false;
}