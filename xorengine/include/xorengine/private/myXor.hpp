#pragma once

#include <cassert>
#include <vector>
#include <cstdint>
#include <unordered_map>
#include <algorithm>
#include <memory>
#include <iostream>
#include <initializer_list>
#include <deque>

#include "xorengine/SolverAdapter.hpp"
#include "prooflogging/xor_proof.hpp"
#include "xorengine/private/BitSet.hpp"

#include "xorengine/private/assert.hpp"
#include "xorengine/private/Logging.hpp"

namespace xorp {
template<typename Types>
class XorMatrix;
template<typename Types>
class XorRow;

template<typename Types>
class WatchEntry {
public:
    XorRow<Types>* row;

    WatchEntry(XorRow<Types>* _row)
        : row(_row)
    {}
};


struct VarLocation {
    size_t matrixId;
    size_t col;
    bool isContained;

    VarLocation(size_t matrixId, size_t col){
        this->matrixId = matrixId;
        this->col = col;
        this->isContained = true;
    }

    VarLocation()
        : matrixId(0)
        , col(0)
        , isContained(false)
    {}
};


template<typename Types>
class XorPropagatorImpl {
private:
    using Solver = xorp::Solver<Types>;

    std::vector<std::vector<typename Types::Var>> col2var;
    std::vector<VarLocation> var2loc;

    std::vector<XorMatrix<Types>> matrices;
    size_t qhead = 0;
    bool isInitialized = false;
    bool mute = false;

    Solver& solver;
public:
    proof::Proof<Types>* proof;

private:
    void addVars(const Xor<Types>& c);
    void addXor(const Xor<Types>& c);

    std::vector<typename Types::Lit> rowToClause(XorRow<Types>& row, bool isReason = false);
public:
    XorPropagatorImpl(Solver& sovler, std::vector<Xor<Types>>& xors, proof::Proof<Types>* proof = nullptr);
    void enqueue(size_t col, bool value, XorRow<Types>& row);
    void conflict(XorRow<Types>& row);
    void propagate();
    void notifyBacktrack();
    void updateAssignment(XorMatrix<Types>& matrix);
    void shrink();

    Reason<typename Types::Lit> computeReason(XorRow<Types>& row, typename Types::Lit propagatedLit);
};

template<typename Types>
class XorMatrix {
public:
    typedef std::vector<WatchEntry<Types>> WatchList;

    XorPropagatorImpl<Types>& propagator;

    // we are using a deque to not invalidate pointers to rows (used
    // in watches) on appending rows
    std::deque<XorRow<Types>> rows;
    size_t matrixId;
    size_t numCols;
    size_t numConstraints;
    std::vector<WatchList> watches;
    std::vector<proof::xr::XorHandle<Types>> col2xorHandle;

    // invariant: unassigned[x] => !value[x]
    BitSet value;
    BitSet unassigned;


    XorMatrix(XorPropagatorImpl<Types>& _propagator, size_t _numCols, size_t _numConstraints, size_t _matrixId)
        : propagator(_propagator)
        , matrixId(_matrixId)
        , numCols(_numCols)
        , numConstraints(_numConstraints)
        , watches(numCols)
        , value(numColsDummy())
        , unassigned(numColsDummy())
    {
        propagator.updateAssignment(*this);
    }

    XorMatrix(XorMatrix&) = delete;
    XorMatrix& operator=(const XorMatrix&) = delete;
    XorMatrix& operator=(XorMatrix&&) = default;
    XorMatrix(XorMatrix&&) = default;
    /**
     * returns true if the literal should still be watched, false otherwise
     */

    void updateAssignment() {
        propagator.updateAssignment(*this);
    }

    void executeDeletion();

    void propagate(size_t col);
    void initWatches();
    std::vector<size_t> shrink();

    size_t numColsDummy() {
        // to avoid special cases we make sure that there are atleast
        // two columns
        return (numCols < 2 ? 2 : numCols);
    }
};

template<typename Types>
class XorRow {
public:
    // invariant: there is no other row that contains basicWatch
    size_t basicWatch;
    size_t nonBasicWatch;

    XorMatrix<Types>* matrix;
    BitSet data;
    BitSet proof;
    bool rhs = false;
    bool del = false;

    std::unique_ptr<proof::xr::XorHandle<Types>> proofHandle = nullptr;

    XorRow(XorMatrix<Types>& _matrix)
        : matrix(&_matrix)
        , data(matrix->numColsDummy())
        // todo: right number of proof cols
        , proof(matrix->numConstraints)
    {}

    XorRow(XorRow<Types>&) = delete;
    XorRow& operator=(const XorRow<Types>&) = delete;
    XorRow& operator=(XorRow<Types>&&) = default;
    XorRow(XorRow<Types>&&) = default;

    bool operator[](size_t col){
        return data[col];
    }

    bool isWatched(size_t col) {
        return basicWatch == col
            || nonBasicWatch == col;
    }

    bool isConflict() {
        XorRow<Types>& self = *this;
        return self[self.basicWatch] == false && this->rhs;
    }

    bool isUnit() {
        XorRow<Types>& self = *this;
        return self.basicWatch == self.nonBasicWatch && self[self.basicWatch] == true;
    }

    size_t otherWatch(size_t col) {
        if (col == basicWatch) {
            return nonBasicWatch;
        } else {
            assert(col == nonBasicWatch);
            return basicWatch;
        }
    }

    void resetProofHandle() {
        if (this->proofHandle) {
            proof::Proof<Types>* proof = this->matrix->propagator.proof;
            if (proof) {
                proof::xr::deleteXor(*proof, *this->proofHandle);
                this->proofHandle = nullptr;
            }
        }
    }

    template <bool init>
    void updateBasicWatch(size_t newBasicWatch){
        LOG(trace) << "XorRow::updateBasicWatch newBasicWatch: " << newBasicWatch << EOM;
        // we want to set a new basic watch, so we need to make sure
        // that it is removed from all other lines to keep our
        // invariant that the basic watch only occurs in one row

        for (XorRow<Types>& row:matrix->rows) {
            if (&row != this) {
                if (row[newBasicWatch]) {
                    assert(init || row[row.nonBasicWatch] == true);
                    row.data ^= this->data;
                    row.rhs ^= this->rhs;
                    row.proof ^= this->proof;
                    row.resetProofHandle();
                    if (!init && row[row.nonBasicWatch] == false) {
                        row.updateWatch(row.nonBasicWatch, true, this->basicWatch);
                        // todo: we might want to remove the entry
                        // from the watchlist, or don't and wait
                        // until that lit is propagated which will
                        // remove it anyway.
                    }
                    assert(!row[newBasicWatch]);
                }
            }
        }

        basicWatch = newBasicWatch;
        matrix->watches[newBasicWatch].push_back(WatchEntry<Types>(this));
    }

    void updateWatch(
        size_t updatedCol,
        bool forceUpdate = false,
        size_t forceAlternativeCol = 0
    ){
        assert(data.size() >= 2);
        LOG(trace) << "XorRow::updateWatch updatedCol: " << updatedCol << EOM;

        if (!isWatched(updatedCol)) {
            return;
        }

        BitSet freeVars(matrix->unassigned);
        freeVars &= this->data;
        // now freeVars[x] is true iff x is in this row and x is unassigned
        // will not contain updatedCol as it should have been set or
        // removed from updateRow before

        // todo: when we have a conflict and the solver backtracks we
        // currently keep going through the watchlist, hence the following assert can be violated
        // assert(!freeVars[updatedCol]);

        assert(freeVars.size() >= 2);

        // we don't want to find the other watch
        freeVars.unset(otherWatch(updatedCol));

        auto it = freeVars.findAny();
        if (it == freeVars.end()) {
            BitSet check(matrix->value);
            check &= this->data;
            // now check[x] is true iff x is in this row and set to true
            bool v = check.popcountMod2() ^ rhs;
            size_t ow = otherWatch(updatedCol);
            // std::cout << ow << " " << (*this)[ow] << " " << matrix->unassigned[ow] << " " << std::endl;
            if ((*this)[ow] && matrix->unassigned[ow]) {
                matrix->propagator.enqueue(ow, v, *this);

                // Here we could also make sure that the propagated
                // column is the basic column, but then this does not
                // really matter as even if it is not the baisc column
                // it will remain set until the basic column is also
                // unset. So lets not do useless work.
            } else {
                if (v) {
                    matrix->propagator.conflict(*this);
                } else {
                    // the constrain is happy
                }
            }

            if (forceUpdate) {
                // the new watch should be in the constraint
                assert((*this)[forceAlternativeCol]);
                // we should never be forced to replace the basic watch
                assert(updatedCol != this->basicWatch);
                // we should only be forced to replace if our nonBasic watch is gone
                assert(!(*this)[this->nonBasicWatch]);

                matrix->watches[forceAlternativeCol].push_back(WatchEntry<Types>(this));
                this->nonBasicWatch = forceAlternativeCol;
                return;
            } else {
                assert((*this)[updatedCol]);
                matrix->watches[updatedCol].push_back(WatchEntry<Types>(this));
                return;
            }
        } else {
            size_t newWatch = it.col();
            if (basicWatch == updatedCol) {
                assert(!forceUpdate);
                updateBasicWatch<false>(newWatch);
            } else {
                nonBasicWatch = newWatch;
                matrix->watches[newWatch].push_back(WatchEntry<Types>(this));
            }
            return;
        }
    }

    void shrink(const std::vector<size_t>& remainingCols){
        throw "not implemented, need to update watcher";

        BitSet rhsChange(matrix->value);
        rhsChange &= this->data;
        // rhsChange[x] iff x is set to true (and hence x is not in
        // remainingCols) and contained in row
        this->rhs ^= rhsChange.popcountMod2();

        for (size_t i = 0; i < remainingCols.size(); i++) {
            data.setTo(i, data[remainingCols[i]]);
        }
        data.resize(remainingCols.size());
    }
};
} // namespace xorp