#pragma once

namespace proof {
    namespace pbp {
        template<typename Types>
        class Proof;
    }

    namespace drat {
        template<typename Types>
        class Proof;
    }

    namespace without {
        template<typename Types>
        class Proof;
    }
}

namespace proof {
    template<typename Types>
    using Proof = proof::pbp::Proof<Types>;
}
