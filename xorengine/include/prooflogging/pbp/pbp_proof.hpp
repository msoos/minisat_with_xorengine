#pragma once

#include <cstdint>
#include <vector>
#include <cassert>
#include <functional>


#include "xorengine/SolverAdapter.hpp"
#include "prooflogging/FastStream.hpp"
#include "pbp_LitPrinter.hpp"

namespace proof {
namespace pbp {
    template<typename Types>
    class Proof {
        using Solver = xorp::Solver<Types>;
        using Var = typename Types::Var;
        using Lit = typename Types::Lit;

        ConstraintId maxId;
        size_t maxVar;
        size_t formulaRead;
        Solver& solver;
        FastStream stream;

    public:
        Proof(std::string filename, Solver& _solver)
            : maxId{0}
            , formulaRead(0)
            , solver(_solver)
            , stream(filename)
        {
            stream << "pseudo-Boolean proof version 2.0\n";
        }

        void setExpectedClauses(int numClauses) {
            stream << "f " << numClauses << "\n";
            maxId.id = numClauses;
        }

        Proof<Types>& operator<<(const Lit& lit) {
            // todo: this does not work if lit is derived from int,
            // because then coefficients will not be printed
            // correctly.
            stream << LitPrinter(solver.getLitPrintName(lit));
            return *this;
        }

        void comment(std::string comment) {
            stream << "* " << comment << "\n";
        }

        Proof<Types>& operator<<(const ConstraintId& id) {
            stream << id.id;
            return *this;
        }

        template<class T>
        Proof<Types>& operator<<(const T& what) {
            stream << what;
            return *this;
        }

        ConstraintId getNextFormulaConstraintId() {
            formulaRead += 1;
            // stream << "l " << formulaRead << "\n";
            return ConstraintId{formulaRead};
        }

        ConstraintId getNewConstraintId() {
            maxId.id += 1;
            return ConstraintId{maxId.id};
        }

        Var newVar() {
            return solver.getFreshVar();
        }

    };

    template<typename Types>
    class ContradictionStep {
        public:
            ContradictionStep(Proof<Types>& proof, ConstraintId id)
            {
                proof << "output NONE\n"
                      << "conclusion UNSAT : " << id << "\n"
                      << "end pseudo-Boolean proof\n";
                // proof << "c " << id << "\n";
            }
    };

    template<typename Types>
    class DeleteStep {
        private:
            Proof<Types>& proof;
        public:
            DeleteStep(Proof<Types>& _proof)
                : proof(_proof)
            {
                proof << "d";
            }

            void addDeletion(const ConstraintId& id) {
                proof << " " << id;
            }

            ~DeleteStep(){
                proof << "\n";
            }
    };

    template<typename Types>
    class PBPRStep {
    private:
        Proof<Types>& proof;
        bool isDegreeSet = false;
        typename Types::Lit mapTo;
        bool isMapping = false;

    public:
        const ConstraintId id;

        PBPRStep(Proof<Types>& _proof, typename Types::Lit _mapTo):
            proof(_proof),
            id(_proof.getNewConstraintId()),
            mapTo(_mapTo)
        {
            isMapping = true;
            proof << "red";
        }

        PBPRStep(Proof<Types>& _proof):
            proof(_proof),
            id(_proof.getNewConstraintId())
        {
            isMapping = false;
            proof << "u";
        }

        ~PBPRStep() {
            assert(isDegreeSet);
        }

        template<typename CoeffType, typename Lit>
        PBPRStep& addTerm(CoeffType coeff, Lit lit) {
            proof << " " << coeff << " " << lit;
            return *this;
        }

        template<typename CoeffType>
        PBPRStep& setDegree(CoeffType degree) {
            assert(!isDegreeSet);
            proof << " >= " << degree << " ;";

            if (isMapping) {
                proof << " ";
                if (xorp::literal::isNegated(mapTo)) {
                    proof << xorp::literal::negated<typename Types::Lit>(mapTo) << " 0";
                } else {
                    proof << mapTo << " 1";
                }
            }
            proof << "\n";
            isDegreeSet = true;
            return *this;
        }
    };

    template<typename Types>
    class EqualityCheck {
    private:
        Proof<Types>& proof;
        bool isDegreeSet = false;

    public:
        EqualityCheck(Proof<Types>& _proof, ConstraintId which):
            proof(_proof)
        {
            proof << "e " << which;
        }

        ~EqualityCheck() {
            assert(isDegreeSet);
        }

        template<typename CoeffType, typename Lit>
        EqualityCheck& addTerm(CoeffType coeff, Lit lit) {
            proof << " " << coeff << " " << lit;
            return *this;
        }

        template<typename CoeffType>
        EqualityCheck& setDegree(CoeffType degree) {
            assert(!isDegreeSet);
            proof << " >= " << degree << " " << ";" << "\n";
            isDegreeSet = true;
            return *this;
        }
    };

    template<typename Types>
    class PolishNotationStep {
    private:
        Proof<Types>& proof;
    public:
        ConstraintId id;

        PolishNotationStep(Proof<Types>& _proof):
            proof(_proof),
            id(_proof.getNewConstraintId())
        {
            proof << "p";
        }

        ~PolishNotationStep(){
            proof << "\n";
        }

        PolishNotationStep& append(ConstraintId geq){
            proof << " " << geq.id;
            return *this;
        }

        template<typename Lit>
        PolishNotationStep& appendLit(Lit lit){
            proof << " " << lit;
            return *this;
        }

        PolishNotationStep& add(){
            proof << " " << "+";
            return *this;
        }

        PolishNotationStep& saturate(){
            proof << " " << "s";
            return *this;
        }

        PolishNotationStep& multiply(uint64_t scalar){
            proof << " " << scalar << " " << "*";
            return *this;
        }

        PolishNotationStep& floor_div(uint64_t scalar){
            proof << " " << scalar << " " << "d";
            return *this;
        }

        void split() {
            proof << " 0\n";
            proof << "p " << this->id;
            this->id = proof.getNewConstraintId();
        }
    };

    template<typename CoeffType, typename Types>
    class LiteralDefinition {
    private:
        using Lit = typename Types::Lit;
    public:
        ConstraintId rightImpl;
        ConstraintId leftImpl;

        LiteralDefinition(
            Proof<Types>& proof,
            Lit def,
            std::vector<CoeffType>&& coeffs,
            std::vector<Lit>&& lits,
            CoeffType degree
        ) {
            {
                assert(lits.size() == coeffs.size());

                PBPRStep<Types> stepRightImpl(proof, ~def);
                for (size_t i = 0; i < lits.size(); i++) {
                    if (coeffs[i] < 0) {
                        lits[i] = ~lits[i];
                        coeffs[i] = -coeffs[i];
                        degree += coeffs[i];
                    }
                    assert(coeffs[i] != 0);
                    stepRightImpl.addTerm(coeffs[i], lits[i]);
                }
                stepRightImpl.addTerm(degree, ~def);
                stepRightImpl.setDegree(degree);
                rightImpl = stepRightImpl.id;
            }

            {
                PBPRStep<Types> stepLeftImpl(proof, def);
                degree = -(degree - 1);
                for (size_t i = 0; i < lits.size(); i++) {
                    degree += coeffs[i];
                    stepLeftImpl.addTerm(coeffs[i], ~lits[i]);
                }
                stepLeftImpl.addTerm(degree, def);
                stepLeftImpl.setDegree(degree);
                leftImpl = stepLeftImpl.id;
            }
        }
    };
}} //closing namespaces