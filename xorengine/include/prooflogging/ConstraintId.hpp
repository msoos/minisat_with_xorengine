#pragma once

#include <iostream>
#include <cstdint>

namespace proof {
    class ConstraintId {
    public:
        uint64_t id;

        static ConstraintId undef() {
            return ConstraintId{0};
        }

        bool operator==(ConstraintId other) {
            return other.id == id;
        }

        bool operator!=(ConstraintId other) {
            return !(*this == other);
        }

        friend std::ostream& operator<< (std::ostream &out, const ConstraintId &node);
    };

    inline std::ostream& operator<< (std::ostream &out, const ConstraintId &node) {
        return out << node.id;
    }
}
