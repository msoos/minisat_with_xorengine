#pragma once

#include "Proof.hpp"
#include "prooflogging/pbp/pbp_drat_rules.hpp"
namespace proof {
    namespace drat_rules = proof::pbp::drat_rules;
}
